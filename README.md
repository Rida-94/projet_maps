# README #

Google maps project.

I. Récupérer le code
-----------------------------------------------

Vous avez deux solutions pour le faire :

1. Via Git, en clonant ce dépôt ;
2. Via le téléchargement du code source en une archive ZIP, à cette adresse : https://bitbucket.org/Rida-94/projet_maps/overview.

II. configuration de la base de donnée
----------------------------------------------

* Créer une base de donnée MySQL en important les dumps SQL database.sql présent dans le répertoire db/ du dossier téléchargé.
* Accédez à votre serveur web, les identifiants par défaut sont admin/admin.